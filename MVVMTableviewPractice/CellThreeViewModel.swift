//
//  CellThreeViewModel.swift
//  MVVMTableviewPractice
//
//  Created by Purushottam Chandra on 17/05/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class CellThreeViewModel {
    
//    var registrationValue : String { get }
    
    var dontKnowAction: (() -> ())?
    
    var getQuoteAction: (() -> ())?
    
    init(dontKnowButtonAction: (() -> ())?, getQuoteButtonAction: (() -> ())?) {
        dontKnowAction = dontKnowButtonAction
        getQuoteAction = getQuoteButtonAction
        
    }
    
    func validate(text: String) -> Bool {
        if text != "" {
            return true
        } else {
            return false
        }
    }
}

extension CellThreeViewModel: CellFunctions {
    static func registerCell(tableview: UITableView) {
        tableview.register(UINib(nibName: "CellThree", bundle: nil), forCellReuseIdentifier: "CellThree")
    }
    
    func cellInstanatiate(tableview: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "CellThree", for: indexPath) as! CellThree
        cell.prepare(viewModel: self)
        return cell
    }
}
